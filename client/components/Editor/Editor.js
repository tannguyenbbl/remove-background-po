import React, { useContext } from "react";
import styles from "./Editor.module.scss";
import Button from "@material-ui/core/Button";
import PublishIcon from "@material-ui/icons/Publish";
import ArtTrackIcon from '@material-ui/icons/ArtTrack';
import { ADD_LAYER, EditorContext } from "../context/EditorProvider";
import LayerImage from "./LayerImage";
import { captureImage, uploadToServer } from "../../helper/func";
import axios from "axios";
import { randomId } from "../../helper/func";

const filterMoveable = (node) => !node.classList.contains("moveable-line");

const Editor = () => {
  const { state, dispatch } = useContext(EditorContext);

  const handlePublish = async () => {
    var node = document.getElementById("editor");

    const formData = await captureImage(node, filterMoveable);
    const result = await uploadToServer(formData);

    console.log({ result });
  };

  const handleRemoveRemovebg = async () => {
    if (state.layers && state.layers.length) {
      const res = await axios.post('/api/image', {
        imageUrl: state.layers[0].config.webformatURL,
        vendor: "removebg"
      }, {
        responseType: "arraybuffer"
      })

      console.log("removebg", res)

      if (res.status === 200) {
        const base64 = Buffer.from(res.data).toString('base64')
        const image = {
          "id": 6786267,
          "largeImageURL": "https://pixabay.com/get/gb0b1cbb28195d54aa1910cf5cde385fdbbc1a5e19baf3a74e51a255fb1a3938a5772b43a46e93e6a86d4762e1950b8d31176803543e861eff178ca9b7c405868_1280.jpg",
          "webformatURL": "https://pixabay.com/get/g3a2bddeb82a9060d035b0602ece5bf22c716844340c24329d2747c2b41211c57dcf8506b62c1a14f7553e2b3a16f50d68050788db277a390201ba22cf0262351_640.jpg",
          "ratio": 1.5,
          "tags": "leopard, mammal, animal",
          "width": 5472,
          "height": 3648
      }

        image.webformatURL = `data:image/png;base64,${base64}`
        const layer = { id: randomId(), config: { ...image } };
        dispatch({ type: ADD_LAYER, payload: layer });
      }
    }
  }


  const handleRemovePixcut = async () => {
    if (state.layers && state.layers.length) {
      const res = await axios.post('/api/image', {
        imageUrl: state.layers[0].config.webformatURL,
        vendor: "pixcut"
      }, {
        responseType: "arraybuffer"
      })

      console.log("pixcut", res)

      if (res.status === 200) {
        const base64 = Buffer.from(res.data).toString('base64')

        const image = {
          "id": 6786267,
          "largeImageURL": "https://pixabay.com/get/gb0b1cbb28195d54aa1910cf5cde385fdbbc1a5e19baf3a74e51a255fb1a3938a5772b43a46e93e6a86d4762e1950b8d31176803543e861eff178ca9b7c405868_1280.jpg",
          "webformatURL": "https://pixabay.com/get/g3a2bddeb82a9060d035b0602ece5bf22c716844340c24329d2747c2b41211c57dcf8506b62c1a14f7553e2b3a16f50d68050788db277a390201ba22cf0262351_640.jpg",
          "ratio": 1.5,
          "tags": "leopard, mammal, animal",
          "width": 5472,
          "height": 3648
      }

        image.webformatURL = `data:image/png;base64,${base64}`
        const layer = { id: randomId(), config: { ...image } };
        dispatch({ type: ADD_LAYER, payload: layer });
      }
    }
  }

  return (
    <div className={styles.container}>
      <div className={styles.actions}>
        <Button
          variant="outlined"
          endIcon={<PublishIcon />}
          onClick={handlePublish}
        >
          Publish
        </Button>
        <Button
          variant="outlined"
          endIcon={<ArtTrackIcon />}
          onClick={handleRemoveRemovebg}
        >
          Remove removebg
        </Button>
        <Button
          variant="outlined"
          endIcon={<ArtTrackIcon />}
          onClick={handleRemovePixcut}
        >
          Remove pixcut
        </Button>
      </div>
      <div className={styles.editor} id="editor">
        {state.layers.map((layer) => (
          <LayerImage key={layer.id} layer={layer} />
        ))}
      </div>
    </div>
  );
};

export default Editor;
