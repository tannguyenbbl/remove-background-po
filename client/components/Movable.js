import React from "react";
import Moveable from "react-moveable";

const Movable = ({ elementRef }) => {
  if (!elementRef.current) {
    return null;
  }

  return (
    <Moveable
      target={elementRef.current}
      container={document.querySelector("#editor")}
      origin={false}
      scalable={false}
      resizable={false}
      warpable={false}
      pinchable={false}
      rotatable={false}
      edge={false}
      keepRatio={true}
      draggable={true}
      throttleDrag={10}
      dragArea={true}
      onDragStart={(e) => {
        const translate = [
          Number.parseFloat(e.target.dataset.translatex),
          Number.parseFloat(e.target.dataset.translatey),
        ];

        console.log(translate);
        e.set(translate);
      }}
      onDrag={(e) => {
        e.target.setAttribute("data-translatex", e.beforeTranslate[0]);
        e.target.setAttribute("data-translatey", e.beforeTranslate[1]);
        e.target.style.transform = `translate(${e.beforeTranslate[0]}px, ${e.beforeTranslate[1]}px)`;
      }}
      onResizeStart={(e) => {
        const translate = [
          Number.parseFloat(e.target.dataset.translatex),
          Number.parseFloat(e.target.dataset.translatey),
        ];
        e.setOrigin(["%", "%"]);
        e.dragStart && e.dragStart.set(translate); 
      }}
      onResize={e => {
        const beforeTranslate = e.drag.beforeTranslate;
    
        e.target.style.width = `${e.width}px`;
        e.target.style.height = `${e.height}px`;
        e.target.style.transform = `translate(${beforeTranslate[0]}px, ${beforeTranslate[1]}px)`;

        e.target.setAttribute("data-width", e.width);
        e.target.setAttribute("data-height", e.height);
        e.target.setAttribute("data-translatex", beforeTranslate[0]);
        e.target.setAttribute("data-translatey", beforeTranslate[1]);
    }}
    />
  );
};

export default Movable;
