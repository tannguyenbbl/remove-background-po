// Next.js API route support: https://nextjs.org/docs/api-routes/introduction
import axios from "axios";
import FormData from "form-data";

const PIX_CUT_KEY = "f852ed03fcc63bcaad085328be08da7b";
const REMOVE_BG_KEY = "G3xDzyTebpyup2sVMAtwcBjz";

const getRandomString = () => {
  return (
    Math.random().toString(36).substring(2) +
    Math.random().toString(36).substring(2)
  );
};

const getFileExtension = (filename) => {
  return filename.split(".").pop();
};

const getFileName = (filename) => {
  const parts = filename.split(".") || [];
  return parts[parts.length - 2] || getRandomString();
};

const handlePixcut = async (imageUrl) => {
  const extension = getFileExtension(imageUrl);
  const localName = `tempVideo-${getFileName(imageUrl)}.${extension}`;

  const imageBuffer = await axios({
    url: imageUrl,
    method: "GET",
    responseType: "arraybuffer",
  }).then((res) => {
    return res.data;
  });

  console.log("pixcut");

  const formData = new FormData();
  formData.append("content", imageBuffer, { filename: localName });

  const config = {
    method: "post",
    url: "https://pixcut.wondershare.com/openapi/api/v1/matting/removebg",
    data: formData,
    headers: {
      appkey: PIX_CUT_KEY,
      "Content-Type": `multipart/form-data; boundary=${formData.getBoundary()}`,
    },
    encoding: null,
    responseType: "arraybuffer",
  };

  const data = await axios(config)
    .then((response) => {
      if (response.status != 200) {
        return console.error("Error:", response.status, response.statusText);
      }

      const data = response.data;
      console.log({ data });
      return data;
    })
    .catch((error) => {
      console.log(error);
    });

  return data;
};

const handleRemovebg = async (imageUrl) => {
  const formData = new FormData();
  formData.append("size", "auto");
  formData.append("image_url", imageUrl);

  console.log("Remove bg");

  const data = await axios({
    method: "post",
    url: "https://api.remove.bg/v1.0/removebg",
    data: formData,
    responseType: "arraybuffer",
    headers: {
      ...formData.getHeaders(),
      "X-Api-Key": REMOVE_BG_KEY,
    },
    encoding: null,
  })
    .then((response) => {
      if (response.status != 200) {
        return console.error("Error:", response.status, response.statusText);
      }

      const data = response.data;
      console.log({ data });
      return data;
    })
    .catch((error) => {
      console.log(error);
    });

  return data;
};

export default async (req, res) => {
  if (req.method === "POST") {
    const body = req.body;
    const imageUrl = body.imageUrl;
    const vendor = body.vendor;

    if (vendor === "pixcut") {
      const data = await handlePixcut(imageUrl);
      res.setHeader("Content-Type", "image/png");
      res.status(200).send(data);

      return;
    }

    if (vendor === "removebg") {
      const data = await handleRemovebg(imageUrl);
      res.status(200).send(data)

      return 
    }
  }
};
